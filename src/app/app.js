//important
import angular from 'angular';

//import internal libs
import components from '../components/components';

//libaries

//styles
import '../style/app.css';
import 'bootstrap/dist/css/bootstrap.css'

let app = () => {
  return {
    template: require('./app.html'),
    controller: 'AppCtrl',
    controllerAs: 'app'
  }
};

class AppCtrl {
  constructor() {
    this.url = 'https://github.com/preboot/angular-webpack';
  }
}

const MODULE_NAME = 'app';

angular.module(MODULE_NAME, [
    components.name,
])
  .directive('app', app)
  .controller('AppCtrl', AppCtrl);

export default MODULE_NAME;