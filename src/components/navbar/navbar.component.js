/**
 * Created by Kacper Włodarczyk on 2016-10-01.
 */

import controller from './navbar.controller';
import template from './navbar.html'

let NavbarComponent = {
    restrict: 'E',
    scope: {},
    template,
    controller,
    controllerAs: 'vm',
    bindToController: true
};

export default NavbarComponent;