/**
 * Created by Kacper Włodarczyk on 2016-10-01.
 */

import angular from 'angular';

import './navbar.css'
import NavBarComponent from './navbar.component'

let NavBarModule = angular
    .module('app.components.navbar', [])
    .component('navbar', NavBarComponent);

export default  NavBarModule;
