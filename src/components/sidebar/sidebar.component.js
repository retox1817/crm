/**
 * Created by Kacper Włodarczyk on 2016-09-29.
 */
import template from './sidebar.html';
import controller from  './sidebar.controller';

let SidebarComponent = {
    restrict: 'E',
    scope: {},
    template,
    controller,
    controllerAs: 'vm',
    bindToController: true
};

export default SidebarComponent;