/**
 * Created by Kacper Włodarczyk on 2016-09-29.
 */
import angular from 'angular';

import './sidebar.css';

import SidebarComponent from './sidebar.component';

let SideBarModule = angular
.module('app.components.sidebar', [])
.component('sidebar', SidebarComponent);

export default SideBarModule;