/**
 * Created by Kacper Włodarczyk on 2016-09-28.
 */
import angular from 'angular';

import SidebarComponent from './sidebar'
import NavBarComponents from './navbar'

export default angular.module('components', [
    SidebarComponent.name,
    NavBarComponents.name
]);